# 创建 自增序列

1. 创建表
 ```
create table point_data (ID integer not null primary key, DATA_VALUE double, POINT_ID integer, YMD integer, HMS integer,FLAG integer, STATUS integer, type integer, data_type integer, SYSINFO_CODE varchar, PROC_MS integer);
```
2. 创建序列
```
create SEQUENCE point_data_sequence start with 1 increment by 1 cache 100000000;

```

3. 插入数据

```
upsert into point_data values     ( NEXT VALUE FOR point_data_sequence, 1234,1235,20160210, 120140, 0, 1024, 2, 1 ,'zhenpu', 599 );
```

4. 注意：
mybitis + phoenix 不能做批量插入,需要重新写业务