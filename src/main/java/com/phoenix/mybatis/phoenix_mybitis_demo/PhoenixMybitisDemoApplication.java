package com.phoenix.mybatis.phoenix_mybitis_demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {
        DataSourceAutoConfiguration.class
})
@MapperScan(value = "com.phoenix.mybatis.phoenix_mybitis_demo.dao")
public class PhoenixMybitisDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PhoenixMybitisDemoApplication.class, args);
    }

}
