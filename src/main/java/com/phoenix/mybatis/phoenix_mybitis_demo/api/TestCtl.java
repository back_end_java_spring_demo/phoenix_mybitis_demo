package com.phoenix.mybatis.phoenix_mybitis_demo.api;

import com.phoenix.mybatis.phoenix_mybitis_demo.bean.DataCommon;
import com.phoenix.mybatis.phoenix_mybitis_demo.bean.ThemeNode;
import com.phoenix.mybatis.phoenix_mybitis_demo.service.MysqlService;
import com.phoenix.mybatis.phoenix_mybitis_demo.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class TestCtl {
    @Autowired
    private TestService testService;
    @Autowired
    private MysqlService mysqlService;

    @RequestMapping("/test")
    public List<DataCommon> getList(){
        return testService.getList();
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public Integer getList(@RequestBody Map<String, Object> params){
        return testService.insertList((List<DataCommon>) params.get("list"));
    }

    @RequestMapping("/mysqlTest")
    public List<ThemeNode> getMysqlList(){
        return mysqlService.getList();
    }
}
