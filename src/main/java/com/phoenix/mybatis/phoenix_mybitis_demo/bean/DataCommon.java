package com.phoenix.mybatis.phoenix_mybitis_demo.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 实时模拟量数据类
 * @author jeffse.jiang
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataCommon {
    private int id;
    private int POINT_ID;
    private double VALUE;
    private int YMD;
    private int HMS;
    private int FLAG;
    private int STATUS;
    private int data_type;
    private int PROC_MS;
    private int type;
    private String SYS_CODE;
}
