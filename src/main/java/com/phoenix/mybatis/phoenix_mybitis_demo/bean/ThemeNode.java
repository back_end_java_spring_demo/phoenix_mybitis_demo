package com.phoenix.mybatis.phoenix_mybitis_demo.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 模拟量数据类
 * @author jeffse.jiang
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ThemeNode {
    private int id;
    private String name;
    private String key;
    private int parent_id;
    private int station_id;
    private String sys_code;
    private int type;
    private String code;
}
