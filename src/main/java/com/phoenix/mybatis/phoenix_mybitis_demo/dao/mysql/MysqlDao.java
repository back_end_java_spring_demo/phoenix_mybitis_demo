package com.phoenix.mybatis.phoenix_mybitis_demo.dao.mysql;

import com.phoenix.mybatis.phoenix_mybitis_demo.bean.DataCommon;
import com.phoenix.mybatis.phoenix_mybitis_demo.bean.ThemeNode;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import javax.annotation.sql.DataSourceDefinition;
import java.util.List;

@Repository
public interface MysqlDao {

    @Select(value = "select * from theme limit 10")
    public List<ThemeNode> getList();
}
