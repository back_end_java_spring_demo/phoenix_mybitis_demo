package com.phoenix.mybatis.phoenix_mybitis_demo.dao.phoenix;

import com.phoenix.mybatis.phoenix_mybitis_demo.bean.DataCommon;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface TestDao {

    @Select("select * from point_data limit 20")
    public List<DataCommon> getList();

    //添加模拟量数据, 此处不能批量加入数据，只能一条一条的插入，目前需要用其他方式解决批量插入的问题
    @Insert({
            "<script>",
            "upsert into point_data values ",
            "<foreach collection='list' item='item' index='index' separator=','>",
            "((NEXT VALUE FOR point_data_sequence + #{index}), #{item.POINT_ID}, #{item.VALUE}, #{item.YMD}, #{item.HMS}, #{item.FLAG}, #{item.STATUS}, #{item.type}, #{item.data_type},#{item.SYS_CODE}, #{item.PROC_MS})",
            "</foreach>",
            "</script>"
    })
    public Integer insertList(List<DataCommon> list);
}
