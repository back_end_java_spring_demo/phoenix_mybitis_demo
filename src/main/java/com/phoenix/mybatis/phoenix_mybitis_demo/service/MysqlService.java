package com.phoenix.mybatis.phoenix_mybitis_demo.service;

import com.phoenix.mybatis.phoenix_mybitis_demo.bean.ThemeNode;
import com.phoenix.mybatis.phoenix_mybitis_demo.dao.mysql.MysqlDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MysqlService {
    @Autowired
    private MysqlDao mysqlDao;

    public List<ThemeNode> getList(){
        return  mysqlDao.getList();
    }
}
