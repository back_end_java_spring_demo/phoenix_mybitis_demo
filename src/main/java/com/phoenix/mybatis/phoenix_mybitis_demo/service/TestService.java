package com.phoenix.mybatis.phoenix_mybitis_demo.service;

import com.phoenix.mybatis.phoenix_mybitis_demo.bean.DataCommon;
import com.phoenix.mybatis.phoenix_mybitis_demo.dao.phoenix.TestDao;
import com.phoenix.mybatis.phoenix_mybitis_demo.utils.MybatisBatchExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestService {
    @Autowired
    private TestDao testDao;
    @Autowired
    private MybatisBatchExecutor mybatisBatchExecutor;

    public List<DataCommon> getList(){
        return  testDao.getList();
    }

    public Integer insertList(List<DataCommon> list){
//        return  testDao.insertList(list);
        mybatisBatchExecutor.insertOrUpdateBatch(list, TestDao.class, (mapper, data)-> {
            testDao.insertList(data);
        });
        return 1;
    }
}
